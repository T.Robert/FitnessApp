# Fitness App
Running on windows, linux and mac with Electron  
To run it execute `npm start` in the `src` folder

## Install
execute `npm install` in the `src` folder

## Testing
execute `npm test` in the `src` folder

## Build (64 bits)
- Windows: `npm run package-win`
- Mac: `npm run package-mac`
- Linux: `npm run package-linux`
  
Build outputs are in the `builds` folder (same level as the `src` folder) 