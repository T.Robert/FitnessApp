// Karma configuration
// Generated on Wed Apr 26 2017 15:03:07 GMT+0200 (Paris, Madrid (heure d’été))

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/bootstrap/dist/js/bootstrap.min.js',
      'node_modules/angular/angular.min.js',
      'node_modules/angular-route/angular-route.min.js',
      'node_modules/slick-carousel/slick/slick.js',
      'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
      'node_modules/angular-drag-and-drop-lists/angular-drag-and-drop-lists.js',
      'node_modules/toastr/build/toastr.min.js',
      'node_modules/socket.io-client/dist/socket.io.js',
      'node_modules/moment/min/moment.min.js',
      'node_modules/fullcalendar/dist/fullcalendar.min.js',
      'node_modules/fullcalendar/dist/gcal.min.js',
      'node_modules/angular-ui-calendar/src/calendar.js',
      'node_modules/angular-prompt/dist/angular-prompt.min.js',
      //App stuff
      'js/Constants.js',
      'js/Index.js',
      'js/RecordModalController.js',
      'js/HomeController.js',
      'js/RunController.js',
      'js/HistoryController.js',
      'js/ProgramEditionController.js',
      'SummaryController',
      //Testing stuff
      'node_modules/angular-mocks/angular-mocks.js',
      'spec/*[sS]pec.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ["spec","htmlDetailed"],
    specReporter: {
      maxLogLines: 5,         // limit number of lines logged per test
      suppressErrorSummary: true,  // do not print error summary
      suppressFailed: false,  // do not print information about failed tests
      suppressPassed: false,  // do not print information about passed tests
      suppressSkipped: true,  // do not print information about skipped tests
      showSpecTiming: false // print the time elapsed for each spec
    },



    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
