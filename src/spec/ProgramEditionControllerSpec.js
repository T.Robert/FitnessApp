/*jshint esversion: 6 */
describe("ProgramEditionController", function() {
  //redefine the log function to prevent any unwanted log from the js function
  console._log = console.log;
  console.log = function() {};

  beforeEach(module('mainApp'));
  var controller, $location, sharedData, $scope, $rootScope;
  beforeEach(inject(function($controller, _$location_, _sharedData_, _$rootScope_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    controller = $controller('ProgramEditionController', { $scope: $scope });
    $location = _$location_;
    sharedData = _sharedData_;
  }));

  describe("adding an exercise should update the total time, the exercises list and the type list", function() {
    beforeEach(function(){
      $scope.trainingProgram = {exercises:[],totalTime:0, types:[]};
    });

    it("check not already existing type update", function(){
      let expectedTypeList = [];
      for(let typeName in gExerciseType){
        let expectedExercise = {time:1, type:typeName, description:"descr_"+typeName, id:"id_"+typeName};
        $scope.addedExercise = JSON.parse(JSON.stringify(expectedExercise));
        $scope.addExercise();
        expectedTypeList.push({id:typeName,qty:1});
      }
      expect($scope.trainingProgram.types).toEqual(expectedTypeList);
    });

    it("check already existing type update", function(){
      let expectedTypeList = [{id:"abdos",qty:2}];
      $scope.addedExercise = {time:1, type:"abdos", description:"descr_", id:"id_"};
      $scope.addExercise();
      $scope.addExercise();
      expect($scope.trainingProgram.types).toEqual(expectedTypeList);
    });

    it("check exercises list update", function(){
      let expectedExercisesList = [{time:1, type:"abdos", description:"descr_1", id:"id_1"},{time:2, type:"bras", description:"descr_2", id:"id_2"}];
      $scope.addedExercise = JSON.parse(JSON.stringify(expectedExercisesList[0]));
      $scope.addExercise();
      $scope.addedExercise = JSON.parse(JSON.stringify(expectedExercisesList[1]));
      $scope.addExercise();
      expect($scope.trainingProgram.exercises).toEqual(expectedExercisesList);
    });

    it("check total time update", function(){
      let expectedTotalTime = 121;
      $scope.addedExercise = {time:60, type:"abdos", description:"descr_", id:"id_"};
      $scope.addExercise();
      $scope.addedExercise = {time:61, type:"abdos", description:"descr_", id:"id_"};
      $scope.addExercise();
      expect($scope.trainingProgram.totalTime).toEqual(expectedTotalTime);
    });
  });

  describe("updating an exercise should update the total time, the exercises list and the type list", function() {
    beforeEach(function(){
      $scope.trainingProgram = {exercises:[
        {time:60, type:"abdos", description:"descr1", id:"id1"},
        {time:60, type:"abdos", description:"descr2", id:"id2"},
        {time:1, type:"bras", description:"descr_3", id:"id_3"}
        ], totalTime:121, types:[{id:"abdos",qty:2},{id:"bras",qty:1}]};
    });

    it("check type list update", function(){
      let expectedTypeList = [{id:"abdos",qty:2},{id:"torse",qty:1}];
      $scope.addedExercise = {time:1, type:"torse", description:"descr_3", id:"id_3"};
      $scope.updateIndex = 2;
      $scope.updateExercise();
      expect($scope.trainingProgram.types).toEqual(expectedTypeList);
    });

    it("check total time update", function(){
      $scope.addedExercise = {time:101, type:"torse", description:"descr_3", id:"id_3"};
      $scope.updateIndex = 2;
      $scope.updateExercise();
      expect($scope.trainingProgram.totalTime).toEqual(221);
    });

    it("check exercise update", function(){
      let expectedExercise = {time:1, type:"torse", description:"descr_99", id:"id_99"};
      $scope.addedExercise = JSON.parse(JSON.stringify(expectedExercise));
      $scope.updateIndex = 2;
      $scope.updateExercise();
      expect($scope.trainingProgram.exercises[2]).toEqual(expectedExercise);
    });
  });
});