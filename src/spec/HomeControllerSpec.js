/*jshint esversion: 6 */
describe("HomeController", function() {
  //redefine the log function to prevent any unwanted log from the js function
  console._log = console.log;
  console.log = function() {};

  beforeEach(module('mainApp'));
  var controller, $location, sharedData, $scope, $rootScope;
  beforeEach(inject(function($controller, _$location_, _sharedData_, _$rootScope_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    controller = $controller('HomeController', { $scope: $scope });
    $location = _$location_;
    sharedData = _sharedData_;
  }));

  it("handle program data", function() {
    $scope.programList = [];
    let programData = {"name":"testProg","totalTime":30,"types":[{"id":"abdos","qty":5},{"id":"cuisses-fessier","qty":1},{"id":"torse","qty":8},{"id":"bras","qty":3},{"id":"gainage","qty":7}],"exercises":[{"id":"Crunch","description":"50 répétitions abdos crunch.","time":5,"type":"abdos"},{"id":"repos","description":"","time":10,"type":"repos"},{"id":"Squat","description":"30 répétitions squat jambes écartées.","time":10,"type":"cuisses-fessier"},{"id":"repos","description":"","time":5,"type":"repos"}]};
    spyOn(toastr, 'success');
    $scope.handleProgramListData(programData);
    expect(toastr.success).toHaveBeenCalled();
    expect($scope.programList).toEqual(programData);
  });

  it("handle program deletion", function() {
    $scope.programList = [];
    $scope.selectedProgramIndex = 1;
    $scope.trainingProgram = {dummy:"dummy"};
    let programData = {"name":"testProg","totalTime":30,"types":[{"id":"abdos","qty":5},{"id":"cuisses-fessier","qty":1},{"id":"torse","qty":8},{"id":"bras","qty":3},{"id":"gainage","qty":7}],"exercises":[{"id":"Crunch","description":"50 répétitions abdos crunch.","time":5,"type":"abdos"},{"id":"repos","description":"","time":10,"type":"repos"},{"id":"Squat","description":"30 répétitions squat jambes écartées.","time":10,"type":"cuisses-fessier"},{"id":"repos","description":"","time":5,"type":"repos"}]};
    spyOn(toastr, 'success');
    $scope.handleProgramDeleted(programData);
    expect(toastr.success).toHaveBeenCalled();
    expect($scope.programList).toEqual(programData);
    expect($scope.trainingProgram).toBeNull();
    expect($scope.selectedProgramIndex).toBeNull();
  });
});