/*jshint esversion: 6 */
describe("HeaderController", function() {
	beforeEach(module('mainApp'));
	var controller, $location, sharedData, $scope, $rootScope;
	beforeEach(inject(function($controller, _$location_, _sharedData_, _$rootScope_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    controller = $controller('HeaderController', { $scope: $scope });
    $location = _$location_;
    sharedData = _sharedData_;
  }));

  it("Tab is active", function() {
    $scope.tabList = [
      {id:"Configuration", active : true, route:"/Home"},
      {id:"Run", active:false, route:"/Run"},
      {id:"Results",active:false, route:"/Results"}
    ];
  	$location.path("/Run");
    $rootScope.$broadcast('$routeChangeSuccess');
  	expect($scope.tabList[1].active).toEqual(true);
  	expect($scope.tabList[0].active).toEqual(false);
  });
});

describe("Factory function", function(){
	beforeEach(module('mainApp'));
  var $filter;
  //inject the factory functions
  beforeEach(inject(function(_$filter_) {
    $filter = _$filter_;
  }));

  it("secondsToDateTime", function() {
  	var date_10s = $filter('secondsToDateTime')(10);
    var date_1min_10s = $filter('secondsToDateTime')(70);
  	expect(date_10s.getSeconds()).toEqual(10);
    expect(date_1min_10s.getMinutes()).toEqual(1);
    expect(date_1min_10s.getSeconds()).toEqual(10);
  });
});