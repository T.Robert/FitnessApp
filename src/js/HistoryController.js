/*jshint esversion: 6 */

//History Controller
mainApp.controller('HistoryController', ['$scope','sharedData', '$timeout', '$location', '$uibModal',
function($scope, sharedData, $timeout, $location, $uibModal){

  //-------------------------------------------------------------------------------------------------------------------
  //COM
  //-------------------------------------------------------------------------------------------------------------------

  socket.removeAllListeners('recordListData');
  socket.on('recordListData', function(arg){$timeout($scope.handleRecordListData,0,true,arg);});

  socket.removeAllListeners('recordDeleted');
  socket.on('recordDeleted', function(arg){$timeout($scope.handleRecordDeleted,0,true,arg);});

  //-------------------------------------------------------------------------------------------------------------------
  //TRIGGERED FUNCTIONS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.goToHome = function(){
    $location.path("/Home");
  };

  $scope.handleCalendarClick = function(eventItem, jsEvent, view){
    $uibModal.open({
      templateUrl: 'RecordModal.html',
      controller: 'recordModalController',
      size: 'lg',
      resolve: {
        selectedRecord: function () {
          return eventItem.recordData;
        },
        selectedUser: function () {
          return $scope.selectedUser;
        }
      }
    });
  };
  
  //-------------------------------------------------------------------------------------------------------------------
  //TOOLS
  //-------------------------------------------------------------------------------------------------------------------
  
  $scope.getRecordsList = function(){
    socket.emit("getHistoryRecordsForUser", $scope.selectedUser._id, "recordListData");
  };

  $scope.handleRecordListData = function(newRecordList){
    //cleaning the existing record list
    $scope.eventSources[0].events.splice(0);
    $scope.recordList = newRecordList;
    $scope.recordList.map(function(record){
      var event = {title: record.program.name, start: record.date, allDay : true, recordData: record, stick: true};
      $scope.eventSources[0].events.push(event);
    });
    $scope.showCalendar = true;
    toastr.success(newRecordList.length + " record(s) retrieved",'Operation successful',{timeOut: 2500});
  };

  $scope.handleRecordDeleted = function(newRecordList){
    $scope.selectedRecord = null;
    $scope.handleRecordListData(newRecordList);
    toastr.success("Record removed",'Operation successful',{timeOut: 2500});
  };

  $scope.initCalendar = function(){
    $scope.uiConfig = {
      calendar:{
        contentHeight: 350,
        height: 350,
        editable: false,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        },
        firstDay : 1,
        views: {
          month: {
              duration: { weeks: 5},
              buttonText: 'Month',
              fixedWeekCount : false
          }
        },
        eventClick: $scope.handleCalendarClick
      }
    };
    $scope.eventSources= [{events:[],className:'eventStyle'}];
  };

  //-------------------------------------------------------------------------------------------------------------------
  //INIT
  //-------------------------------------------------------------------------------------------------------------------

  $scope.init = function () {
    $scope.selectedRecord = null;
    $scope.showCalendar = false;
    $scope.initCalendar();
    $scope.recordList = [];
    $scope.selectedUser = sharedData.selectedUser;
    $scope.getRecordsList();

  };
  $scope.init();

}]);
//end History Controller