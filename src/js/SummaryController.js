/*jshint esversion: 6 */

//Summary Controller
mainApp.controller('SummaryController', ['$scope','sharedData', '$location','$timeout', 'prompt',
function($scope,sharedData, $location,$timeout, prompt){

  //-------------------------------------------------------------------------------------------------------------------
  //COM
  //-------------------------------------------------------------------------------------------------------------------

  socket.removeAllListeners('historyRecordAdded');
  socket.on('historyRecordAdded', function(arg){$timeout($scope.handleHistoryRecordAdded,0,true,arg);});

  //-------------------------------------------------------------------------------------------------------------------
  //TRIGGERED FUNCTIONS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.goToHome = function(){
    $location.path("/Home");
  };

  $scope.triggerHistoryPush = function(){
    prompt({
      "title": 'Saving session history',
      "message": 'This is your session summary. Do you want to save it to your user history ?',
      "buttons": [
        {
          "label": "Hmmm No",
          "cancel": true,
          "primary": false
        },
        {
          "label": "Save it !",
          "cancel": false,
          "primary": true
        }
      ]
    }).then(function(){
      $scope.saveRunHistory();
    });
  };

  //-------------------------------------------------------------------------------------------------------------------
  //TOOLS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.saveRunHistory = function(){
    let dataObj = JSON.parse(angular.toJson($scope.runHistoryToPush)); //removing the $$hashkey created by angular
    socket.emit("addHistoryRecordToDb", dataObj, "historyRecordAdded");
  };

  $scope.handleHistoryRecordAdded = function(newRecordList){
    toastr.success("Session saved to history",'Operation successful',{timeOut: 2500});
  };

  //-------------------------------------------------------------------------------------------------------------------
  //INIT
  //-------------------------------------------------------------------------------------------------------------------

  $scope.init = function () {
    $scope.runHistoryToPush = sharedData.runHistoryToPush;
  };
  $scope.init();

}]);
//end Summary Controller