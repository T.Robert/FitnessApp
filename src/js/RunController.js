/*jshint esversion: 6 */

//Run Controller
mainApp.controller('RunController', ['$scope','sharedData', '$location','$timeout', 'prompt',
function($scope,sharedData, $location,$timeout, prompt){

  //-------------------------------------------------------------------------------------------------------------------
  //COM
  //-------------------------------------------------------------------------------------------------------------------

  socket.removeAllListeners('historyRecordAdded');
  socket.on('historyRecordAdded', function(arg){$timeout($scope.handleHistoryRecordAdded,0,true,arg);});

  //-------------------------------------------------------------------------------------------------------------------
  //TRIGGERED FUNCTIONS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.goToHome = function(){
    //Saving the autoplay status
    sharedData.autoplay = $scope.autoplay;
    $location.path("/Home");
  };

  $scope.goToSessionSummary =function(){
    sharedData.runHistoryToPush = $scope.runHistoryToPush;
    $location.path("/Summary");
  };

  //Function called at the beginning of the program or when the user press the resume button after a pause.
  //It handle the global timer start and call startExercise just after
  $scope.startProgram = function(){
    let resumeExercise;
    //checking if its a resume or not
    if($scope.runStatus !== 'paused'){
      //if it's not a resume, we reset the exercise slider and the timer
      $scope.resetGlobalTimer();
      $scope.resetSlider();
      $scope.runHistoryToPush = null;
      resumeExercise = false;
    }
    else{
      //if it's a resume, we just set the variable for the startExercise function
      resumeExercise = true;
    }
    //specific case when we are in "autoplay : off" => we will start the next exercise but the global timer is already running
    if(!$scope.globalTimerRunning){
      //defining the starting time
      // if we have a pause time, that means that the program has been paused, thus we don't reset but
      // update the starting time to take in account the pause
      if($scope.globalStartTime && $scope.globalPausetime){
        var elapsedTime = $scope.globalPausetime - $scope.globalStartTime;
        $scope.globalStartTime = moment() - elapsedTime;
        $scope.globalPausetime = null;
      }
      else{
        $scope.globalStartTime = moment();
      }
      $scope.startGlobalTimer();
    }
    $scope.runStatus = 'running';
    $scope.startExercise(resumeExercise);
  };

  //Function called at the beginning of each exercise (or when resuming it).
  $scope.startExercise = function(resumeExercise=false){
    $scope.exerciseStartTime = moment();
    //if we don't resume the exercise, we update the timer and the data
    if(!resumeExercise){
      $scope.halfTimeAudioPlayed = false;
      $scope.exerciseOffsetTime = 0;
      //getting current slide index
      $scope.currentIndex = $('#horizontalList').slick("slickCurrentSlide");
      $scope.currentExercise = $scope.trainingProgram.exercises[$scope.currentIndex];
      $scope.exerciseTime = $scope.trainingProgram.exercises[$scope.currentIndex].time;
    }
    else{
      //if we resume the exercise, we need to set an offset for the timer
      $scope.exerciseOffsetTime = $scope.currentExercise.time - $scope.exerciseTime;
    }
    //starting the exercise timer
    $scope.startTimer();
  };

  $scope.stop = function(){
    $scope.runStatus = 'stopped';
    $timeout.cancel($scope.timer);
    $scope.resetGlobalTimer();
    $scope.resetSlider();
    $scope.currentIndex = 0;
    $scope.exerciseTime = $scope.trainingProgram.exercises[$scope.currentIndex].time;
    $scope.currentExercise = $scope.trainingProgram.exercises[$scope.currentIndex];

  };

  $scope.pause = function(){
    $scope.runStatus = 'paused';
    $timeout.cancel($scope.timer);
    $scope.pauseGlobalTimer();
  };

  //skipping an exercise
  $scope.skip = function(){
    $scope.currentExercise.skipped = true;
    if($scope.currentIndex+1 < $scope.trainingProgram.exercises.length){
      $timeout.cancel($scope.timer);
      $scope.slideNext();
      $scope.currentIndex = $('#horizontalList').slick("slickCurrentSlide");
      $scope.exerciseTime = $scope.trainingProgram.exercises[$scope.currentIndex].time;
      $scope.currentExercise = $scope.trainingProgram.exercises[$scope.currentIndex];
      $scope.exerciseStartTime = moment();
      $scope.halfTimeAudioPlayed = false;
      $scope.exerciseOffsetTime = 0;
      if($scope.runStatus === "running"){
        $scope.startTimer();
        if(!$scope.autoplay){
          //must do a start then pause to refresh the displayed exercise and exercise remaining time
          $scope.runStatus = 'paused';
          $timeout.cancel($scope.timer);
        }
      }
      else{
        //setting the global status tu pause to allow starting from this point
        $scope.runStatus = 'paused';
      }
    }
    else{
      $timeout.cancel($scope.timer);
      $scope.exerciseTime = 0;
      $scope.sessionFinished();
    }
  };

  //function called automatically when an exercise finish
  $scope.exerciseFinished = function(){
    if($scope.currentIndex+1 < $scope.trainingProgram.exercises.length){
      $scope.audioExerciseFinished.play();
      $scope.slideNext();
      $scope.startExercise();
      //checking for autoplay setting
      if(!$scope.autoplay){
        //must do a start then pause to refresh the displayed exercise and exercise remaining time
          $scope.runStatus = 'paused';
          $timeout.cancel($scope.timer);
      }
    }
    else{
      $scope.sessionFinished();
    }
  };

  //-------------------------------------------------------------------------------------------------------------------
  //TOOLS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.slideNext= function(){
    $('#horizontalList').slick("slickNext");
  };

  $scope.resetSlider =function(){
    $('#horizontalList').slick("slickGoTo",0);
  };

  $scope.startTimer = function(){
    $scope.timer = $timeout(function() {
      $scope.exerciseTime = $scope.currentExercise.time - Math.floor((moment() - $scope.exerciseStartTime)/1000) - $scope.exerciseOffsetTime;
      if($scope.exerciseTime > 0){
        //checking for half-time
        var roundedHalfTime = Math.round($scope.currentExercise.time/2);
        if(!$scope.halfTimeAudioPlayed && $scope.exerciseTime === roundedHalfTime){
          $scope.audioHalfTime.play();
          $scope.halfTimeAudioPlayed = true;
        }
        $scope.startTimer();
      }
      else{
        $scope.exerciseFinished();
      }
    }, 250);
  };

  $scope.startGlobalTimer = function(){
    $scope.globalTimerRunning = true;
    $scope.globalTimer = $timeout(function() {
      //getting the elapsed time in seconds
      $scope.elapsedTime = Math.floor((moment() - $scope.globalStartTime)/1000);
      $scope.startGlobalTimer();
    }, 250);
  };

  $scope.pauseGlobalTimer = function(){
    $scope.globalPausetime = moment();
    $scope.stopGlobalTimer();
  };

  $scope.stopGlobalTimer = function(){
    $timeout.cancel($scope.globalTimer);
    $scope.globalTimerRunning = false;
  };

  $scope.resetGlobalTimer = function(){
    $scope.stopGlobalTimer();
    //resetting the elapsed time
    $scope.elapsedTime = 0;
  };

  $scope.sessionFinished = function(){
    $scope.audioSessionFinished.play();
    $scope.stopGlobalTimer();
    $scope.prepareRunHistory();
    $timeout(function() {
      $scope.runStatus = "finished";
    });
  };

  $scope.prepareRunHistory = function(){
    $scope.runHistoryToPush = {
      date:new Date(),
      user:sharedData.selectedUser,
      elapsedTime: $scope.elapsedTime,
      program:JSON.parse(JSON.stringify($scope.trainingProgram)),
      skippedExercisesQty : 0
    };
    //building a light exercises list, whitout description or type
    $scope.runHistoryToPush.program.exercises.map(function(exercise){
      if(exercise.skipped && exercise.type !== gRestType){
        $scope.runHistoryToPush.skippedExercisesQty += 1;
      }
    });
    //revoving angular specific keys starting with $
    $scope.runHistoryToPush = JSON.parse(angular.toJson($scope.runHistoryToPush));
  };

  //-------------------------------------------------------------------------------------------------------------------
  //INIT
  //-------------------------------------------------------------------------------------------------------------------

  $scope.init = function () {
    $scope.carouselStyle = {"visibility":"hidden"};
    $(document).ready(function(){
      $('#horizontalList').slick({
        centerMode: true,
        centerPadding: '25%',
        slidesToShow: 1,
        accessibility: false,
        infinite: false,
        draggable: false
      });
      $timeout(function(){
        $scope.slideStyle = {"height":$('.slick-track').height()+"px"};
        $scope.carouselStyle = {"visibility":"initial"};
      });
    });
    $scope.globalTimerRunning = false;
    $scope.currentIndex = 0;
    $scope.elapsedTime = 0;
    $scope.autoplay = sharedData.autoplay;
    $scope.userName = sharedData.selectedUser.name;
    //deep copy of the training program to allow modification on it without affecting other pages
    $scope.trainingProgram = JSON.parse(JSON.stringify(sharedData.trainingProgram));
    $scope.exerciseTime = $scope.trainingProgram.exercises[$scope.currentIndex].time;
    $scope.currentExercise = $scope.trainingProgram.exercises[$scope.currentIndex];
    $scope.audioExerciseFinished = new Audio('./resources/sound/3ding.mp3');
    $scope.audioSessionFinished = new Audio('./resources/sound/4ding.mp3');
    $scope.audioHalfTime = new Audio('./resources/sound/ding.mp3');
  };
  $scope.init();

}]);
//end Run Controller