// jshint esversion:6

const {app, Menu, BrowserWindow} = require('electron');
const express = require('express');
const expressServer = express();
const port = 8082;

const path = require('path');
const url = require('url');
const io = require('socket.io')(7751);
const dbTools = require("./DBTools.js");

var template = [
    {
    label: "App",
    submenu: [
        { label: "Quit", accelerator: "CmdOrCtrl+Q", role: "quit" }
    ]},
    {
    label: "Edit",
    submenu: [
        { label: "Undo", accelerator: "CmdOrCtrl+Z", role: "undo" },
        { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", role: "redo" },
        { type: "separator" },
        { label: "Cut", accelerator: "CmdOrCtrl+X", role: "cut" },
        { label: "Copy", accelerator: "CmdOrCtrl+C", role: "copy" },
        { label: "Paste", accelerator: "CmdOrCtrl+V", role: "paste" },
        { label: "Select All", accelerator: "CmdOrCtrl+A", role: "selectall" }
    ]},
    {
    label: "dev",
    submenu: [
        { label: "Dev Tools", accelerator: "CmdOrCtrl+Shift+I", role: "toggledevtools" },
        { label: "Reload", accelerator: "CmdOrCtrl+R", role: "forcereload" }
    ]}
];

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

//WEB SOCKET
//setup hmiNamespace event handlers
io.on('connection', function(client){
  console.log("client connected");

  client.on('getProgramsFromDb', function(response){
    console.log("client -> Server : getProgramsFromDb");
    dbTools.lookForPrograms(client, response);   
  });

  client.on('saveProgramToDb', function(programObj, response){
    console.log("client -> Server : saveProgramToDb");
    dbTools.writeProgramData(programObj, client, response);
  });

  client.on('deleteProgramFromDb', function(programId, response){
    console.log("client -> Server : deleteProgramFromDb");
    dbTools.deleteProgram(programId, client, response);
  });

  client.on('getUsersFromDb', function(response){
    console.log("client -> Server : getUsersFromDb");
    dbTools.lookForUsers(client, response);
  });

  client.on('addUserToDb', function(userObj, response){
    console.log("client -> Server : addUserToDb");
    dbTools.addUser(userObj, client, response);
  });

  client.on('deleteUserFromDb', function(userId, response){
    console.log("client -> Server : deleteUserFromDb");
    dbTools.deleteUser(userId, client, response);
  });

  client.on('getHistoryRecordsForUser', function(userId, response){
    console.log("client -> Server : getHistoryRecordsForUser");
    dbTools.lookForHistoryRecords(userId, client, response);
  });

  client.on('addHistoryRecordToDb', function(recordObj, response){
    console.log("client -> Server : addHistoryRecordToDb");
    dbTools.addHistoryRecord(recordObj, client, response);
  });

  client.on('deleteHistoryRecordFromDb', function(recordId, userId, response){
    console.log("client -> Server : deleteHistoryRecordFromDb");
    dbTools.deleteHistoryRecord(recordId, userId, client, response);
  });

  client.on('minimize', function(){
    console.log("client -> Server : minmize");
    mainWindow.minimize();
  });

  client.on('close', function(){
    console.log("client -> Server : close");
    mainWindow.close();
  });

  //registering a disconnect event for this specific socket
  client.on('disconnect', function(){
    console.log("client disconnected");
  });
});

//ELECTRON WINDOW
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    minWidth: 1024, 
    minHeight: 768,
    width: 1024,
    height: 768,
    frame: false,
    webPreferences :{
      nodeIntegration :false,
      preload: path.join(__dirname, 'ClientPreload.js')
    }
  });

  // and load the index.html of the app.
  mainWindow.loadURL('http://127.0.0.1:8082/');

  // Open the DevTools.
  /*mainWindow.webContents.openDevTools();*/

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

function startExpressServer () {
  expressServer.use(express.static(path.join(__dirname, '..')));
  expressServer.listen(port, function(){
    console.log('FitnessApp listening on port '+port+'!');
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function(){
  startExpressServer();
  createWindow();
  //the menu need to be set after the app is reday to be effective
  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
