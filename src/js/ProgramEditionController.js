/*jshint esversion: 6 */

//ProgramEdition Controller
mainApp.controller('ProgramEditionController', ['$scope','sharedData', '$location', '$timeout', '$uibModal', 'prompt',
function($scope, sharedData, $location, $timeout, $uibModal, prompt){

  //-------------------------------------------------------------------------------------------------------------------
  //COM
  //-------------------------------------------------------------------------------------------------------------------

  socket.removeAllListeners('programSaved');
  socket.on('programSaved', function(arg){$timeout($scope.handleProgramSaved,0,true,arg);});


  //-------------------------------------------------------------------------------------------------------------------
  //TRIGGERED FUNCTIONS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.goToHome = function(){
    let programCopy = JSON.parse(angular.toJson($scope.trainingProgram));
    angular.forEach(programCopy.exercises,function(exercise) { delete exercise.selected;});
    if( angular.toJson(programCopy)!== angular.toJson(sharedData.programToEdit || {})){
      $scope.showUnsavedDialog();
    }
    else{
      $location.path("/Home");
    }
  };

  $scope.addExercise = function(){
    //add the exercise to the list
    $scope.trainingProgram.exercises.push(Object.assign({},$scope.addedExercise));
    $scope.updateGlobalProgramStats();
  };

  $scope.saveProgram = function(){
    let programCopy = JSON.parse(angular.toJson($scope.trainingProgram));
    angular.forEach(programCopy.exercises,function(exercise) { delete exercise.selected;});
    socket.emit("saveProgramToDb", programCopy, "programSaved");
    sharedData.programToEdit = programCopy;
  };

  $scope.editExercise = function($event, id, time, description, type, index){
    if($scope.updateIndex != index){
      $scope.addedExercise.id = id;
      $scope.addedExercise.time = time;
      $scope.addedExercise.description = description;
      $scope.addedExercise.type = type;
      $scope.updateIndex = index;

      $scope.showUpdateBtn = true;
    }
    else{
      $scope.showUpdateBtn = false;
      $scope.addedExercise = {type:$scope.exerciseType[0], time:1};
      $scope.updateIndex = null;
    }
  };

  $scope.updateExercise = function(){
    //updating the exercise list
    $scope.trainingProgram.exercises[$scope.updateIndex] = Object.assign({},$scope.addedExercise);
    $scope.showUpdateBtn = false;
    $scope.updateIndex = null;
    $scope.addedExercise = {type:$scope.exerciseType[0], time:1};
    $scope.updateGlobalProgramStats();
  };

  $scope.selectAllExercises = function(){
    angular.forEach($scope.trainingProgram.exercises,function(exercise) { exercise.selected = true; });
  };

  $scope.duplicateExercises = function(){
    var exercisesToDuplicate = [];
    $scope.trainingProgram.exercises.map(function(exercise){
      if(exercise.selected){
        exercisesToDuplicate.push(JSON.parse(angular.toJson(exercise))); // deep copy
      }
    });
    angular.forEach(exercisesToDuplicate,function(exercise) { exercise.selected = false; });
    $scope.trainingProgram.exercises = $scope.trainingProgram.exercises.concat(exercisesToDuplicate);
    $scope.updateGlobalProgramStats();
  };

  $scope.cancelAddOrUpdate = function(){
    //reset the add exercise
    $scope.addedExercise = {type:$scope.exerciseType[0], time:1};
    $scope.showUpdateBtn = false;
    $scope.updateIndex = null;
    angular.forEach($scope.trainingProgram.exercises,function(exercise) { exercise.selected = false; });
  };

  //DRAGGING

  $scope.getSelectedItemsIncluding = function(item, trainingProgram) {
    item.selected = true;
    return trainingProgram.exercises.filter(function(item) { return item.selected; });
  };

  $scope.dragStartCallback = function(){
    $scope.currenltyDragging = true;
    $scope.showUpdateBtn = false;
    $scope.showTrashCan = true;
  };

  $scope.dropCallback = function(trainingProgram, selectedItems, index){
    //removing the selected state of your selected items before adding
    angular.forEach(selectedItems, function(item) { item.selected = false; });
    trainingProgram.exercises = trainingProgram.exercises.slice(0, index)
                .concat(selectedItems)
                .concat(trainingProgram.exercises.slice(index));
    return true; //By returning true, the dnd-list directive won't do the insertion itself.
  };

  $scope.moveCallback = function(trainingProgram){
    trainingProgram.exercises = trainingProgram.exercises.filter(function(item) { return !item.selected; });
  };

  $scope.dragEndCallback = function(){
    $scope.currenltyDragging = false;
    $scope.showTrashCan = false;
  };

  $scope.selectUnselectExercise = function(item){
    item.selected = !item.selected;
  };

  //-------------------------------------------------------------------------------------------------------------------
  //TOOLS
  //-------------------------------------------------------------------------------------------------------------------
  //copy the given type array and update the copy with the new given type.
  //if the given type is the same as the global variable gRestType, nothing is done.
  $scope.addOrUpdateType = function(typeList, newType){
    if(newType === gRestType)
      return typeList;
    var copiedTypeList = JSON.parse(angular.toJson(typeList));
    var foundType = false;
    for(let typeObj of copiedTypeList){
      if(typeObj.id === newType){
        typeObj.qty = (typeObj.qty || 0) + 1;
        foundType = true;
        break;
      }
    }
    if(!foundType){
      copiedTypeList.push({id:newType,qty:1});
    }
    return copiedTypeList;
  };

  $scope.decreaseType = function(typeList, typeToDecrease){
    if(typeToDecrease === gRestType)
      return typeList;
    var copiedTypeList = JSON.parse(angular.toJson(typeList));
    for(let i=0; i<copiedTypeList.length; i++){
      let typeObj = copiedTypeList[i];
      if(typeObj.id === typeToDecrease){
        typeObj.qty -= 1;
        //removing the entry if the qty is 0
        if(typeObj.qty === 0){
          copiedTypeList.splice(i,1);
        }
        break;
      }
    }
    return copiedTypeList;
  };

  $scope.handleProgramSaved = function(newProgramList){
    toastr.success("Program saved",'Operation successful',{timeOut: 2500});
    sharedData.programList = newProgramList;
  };

  $scope.showUnsavedDialog = function () {
    prompt({
      title: 'Unsaved Changes',
      message: 'Your are going to loose all unsaved changes. Are you sure you want to leave ?',
      "buttons": [
        {
          "label": "Stay here",
          "cancel": true,
          "primary": false
        },
        {
          "label": "Leave",
          "cancel": false,
          "primary": true
        }
      ]
    }).then(function(){
      $location.path("/Home"); 
    });
  };

  $scope.countSelected = function(){
    var count = 0;
    $scope.trainingProgram.exercises.map(function(exercise){
      if(exercise.selected){
        count+=1;
      }
    });
    return count;
  };

  $scope.updateGlobalProgramStats = function(){
    let progTypes = [];
    let progTime = 0;
    angular.forEach($scope.trainingProgram.exercises,function(exercise) {
      progTypes = $scope.addOrUpdateType(progTypes, exercise.type);
      progTime += exercise.time;
    });
    $scope.trainingProgram.totalTime = progTime;
    $scope.trainingProgram.types = progTypes;
  };

  //-------------------------------------------------------------------------------------------------------------------
  //INIT
  //-------------------------------------------------------------------------------------------------------------------

  $scope.init = function () {
    //deep copy
    $scope.trainingProgram = JSON.parse(angular.toJson(sharedData.programToEdit || {}));
    //init the exercises list
    $scope.trainingProgram.exercises = $scope.trainingProgram.exercises || [];
    angular.forEach($scope.trainingProgram.exercises,function(exercise) { exercise.selected = false; });
    //init the program total time
    $scope.trainingProgram.totalTime = $scope.trainingProgram.totalTime || 0;
    //init the program type lsit
    $scope.trainingProgram.types = $scope.trainingProgram.types || [];

    $scope.title = (!sharedData.programToEdit) ? "Program Creation" : "Program Edition";
    $scope.exerciseType = gExerciseType;
    $scope.addedExercise = {type:$scope.exerciseType[0], time:1};

    $scope.showUpdateBtn = false;
    $scope.showTrashCan = false;
  };
  $scope.init();

}]);
//end ProgramEdition Controller