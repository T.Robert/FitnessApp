// jshint esversion:6

//Toastr specific config
toastr.options.progressBar = true;
toastr.options.preventDuplicates = false;
toastr.options.showMethod ='slideDown';
toastr.options.hideMethod = 'slideUp';
toastr.options.closeMethod = 'slideUp';
toastr.options.showDuration = "300";
toastr.options.hideDuration = "200";

//Socket.io object
var socket;

//---------------------------------------------------------------------------
// ANGULAR
//---------------------------------------------------------------------------
var mainApp = angular.module('mainApp', ['ngRoute','ui.bootstrap','dndLists','ui.calendar', 'cgPrompt']);

//*************//
//SETTINGS
//*************//
//Set the router config
mainApp.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider
      .when('/Home', {
        templateUrl: './Home.html',
        controller: 'HomeController'
      })
      .when('/Run', {
        templateUrl: './Run.html',
        controller: 'RunController'
      })
      .when('/ProgramEdition',{
        templateUrl: './ProgramEdition.html',
        controller: 'ProgramEditionController'
      })
      .when('/History',{
        templateUrl: './History.html',
        controller: 'HistoryController'
      })
      .when('/Summary',{
        templateUrl: './Summary.html',
        controller: 'SummaryController'
      })
      .otherwise({
        redirectTo: '/Home'
      });
      $locationProvider.html5Mode({
        enabled: false,
        requireBase: false
      });
}]);

//code to avoid a warning in the console
mainApp.config(['$qProvider', function ($qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
}]);
//*************//
//SHARED DATA
//*************//
mainApp.factory('sharedData', function() {
  return {
    programList: null,
    trainingProgram: null,
    selectedProgramIndex: null,
    selectedUser: null,
    userList: null,
    autoplay : true        
  };
});

mainApp.filter('secondsToDateTime', [function() {
  return function(seconds) {
    var dateElement = new Date(1970, 0, 1);
    dateElement.setSeconds(seconds);
    return dateElement;
  };
}]);

//*************//
//CONTROLLERS
//*************//
//--------------
//Tab controller
mainApp.controller('HeaderController', ['$scope', '$timeout', 'sharedData', '$location',
    function($scope, $timeout, sharedData, $location) {
  
  //-------------------------------------------------------------------------------------------------------------------
  //TRIGGERED FUNCTIONS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.handleRouteChange = function(event, next, current){
    var currentLocation = $location.path();
    for(i=0; i<$scope.tabList.length; i++){
      if($scope.tabList[i].route === currentLocation){
        $scope.tabList[i].active = true;
      }
      else{
        $scope.tabList[i].active = false;
      }
    }
  };

  $scope.close = function(){
    socket.emit("close");
  };

  $scope.minimize = function(){
    socket.emit("minimize");
  };
  //-------------------------------------------------------------------------------------------------------------------
  //COM
  //-------------------------------------------------------------------------------------------------------------------
  $scope.registerWsHandlers = function(){
    socket.on('errorMsg', function (msg, err) {
      toastr.error(err,msg,{timeOut: 0, extendedTimeOut:0,positionClass: "toast-top-center"});
    });
  };

  //-------------------------------------------------------------------------------------------------------------------
  //TOOLS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.initCom = function(){
    var host = "127.0.0.1"; 
    //checking if we access to the hmi in remote, in this case we need to update the host variable
    var addrRegExp = /[^\/]+\/\/(.+):.+/gi;
    var match = addrRegExp.exec(window.location.href);
    if(match !== null){
      host = match[1];
    }

    socket = io("http://"+host+":7751");
    //register socket event handlers
    $scope.webSocketIsUp = false;

    socket.on('connect', function () {
      console.log("socket connected");
      $timeout(function(){$scope.webSocketIsUp = true;});
    });

    socket.on('disconnect', function () {
      console.log("socket disconnected");
      $timeout(function(){$scope.webSocketIsUp = false;});
    });

    socket.on('error', function () {
      console.log("socket error");
    });
  };

  //-------------------------------------------------------------------------------------------------------------------
  //INIT
  //-------------------------------------------------------------------------------------------------------------------

  $scope.init = function(){
    $scope.initCom();
    $scope.registerWsHandlers();
    // this variable is set in the preload script, it is only accessible to the electron window, not the the remote access
    $scope.runInElectron = window.runInElectron;
    $scope.tabList = [{id:"Configuration", active : true, route:"/Home"}, {id:"Run", active:false, route:"/Run"}, {id:"History",active:false, route:"/History"}];
    $scope.$on('$routeChangeSuccess', $scope.handleRouteChange);
  };
  $scope.init();

}]);
//End header controller