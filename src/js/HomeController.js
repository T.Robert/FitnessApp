/*jshint esversion: 6 */

//Home Controller
mainApp.controller('HomeController', ['$scope','sharedData', '$location', '$uibModal', '$timeout',
function($scope,sharedData, $location, $uibModal, $timeout){

  //-------------------------------------------------------------------------------------------------------------------
  //COM
  //-------------------------------------------------------------------------------------------------------------------

  socket.removeAllListeners('programListData');
  socket.on('programListData', function(arg){$timeout($scope.handleProgramListData,0,true,arg);});

  socket.removeAllListeners('programDeleted');
  socket.on('programDeleted', function(arg){$timeout($scope.handleProgramDeleted,0,true,arg);});

  socket.removeAllListeners('userListData');
  socket.on('userListData', function(arg){$timeout($scope.handleUserListData,0,true,arg);});

  socket.removeAllListeners('userAdded');
  socket.on('userAdded', function(arg){$timeout($scope.handleUserAdded,0,true,arg);});

  socket.removeAllListeners('userDeleted');
  socket.on('userDeleted', function(arg){$timeout($scope.handleUserDeleted,0,true,arg);});

  //-------------------------------------------------------------------------------------------------------------------
  //TRIGGERED FUNCTIONS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.updateTrainingProgram = function(){
    $scope.trainingProgram = $scope.programList[$scope.selectedProgramIndex];
  };

  $scope.deleteProgram = function(){
    socket.emit("deleteProgramFromDb", $scope.programList[$scope.selectedProgramIndex]._id, "programDeleted");
  };

  $scope.goToRun = function(){
    sharedData.selectedUser = $scope.selectedUser;
    sharedData.selectedProgramIndex = $scope.selectedProgramIndex;
    sharedData.programList = $scope.programList;
    sharedData.trainingProgram = $scope.trainingProgram;
    sharedData.userList = $scope.userList;
    $location.path("/Run");
  };

  $scope.goToHistory = function(){
    sharedData.selectedUser = $scope.selectedUser;
    sharedData.selectedProgramIndex = $scope.selectedProgramIndex;
    sharedData.programList = $scope.programList;
    sharedData.trainingProgram = $scope.trainingProgram;
    sharedData.userList = $scope.userList;
    $location.path("/History");
  };

  $scope.goToProgramEdition = function (createNew) {
    sharedData.programToEdit = createNew ? null : $scope.trainingProgram;
    sharedData.trainingProgram = $scope.trainingProgram;
    sharedData.selectedProgramIndex = $scope.selectedProgramIndex;
    sharedData.selectedUser = $scope.selectedUser;
    sharedData.userList = $scope.userList;
    $location.path("/ProgramEdition");
  };

  $scope.toggleUserCreation = function(showInput){
    $scope.showUserInput = showInput;
    if(!showInput){
      $scope.newUser = {name:""};
    }
  };

  $scope.addUser = function(){
    socket.emit("addUserToDb", $scope.newUser, "userAdded");
  };

  $scope.removeUser = function(){
    socket.emit("deleteUserFromDb", $scope.selectedUser._id, "userDeleted");
  };

  //-------------------------------------------------------------------------------------------------------------------
  //TOOLS
  //-------------------------------------------------------------------------------------------------------------------
  
  $scope.handleProgramListData = function(programList){
    $scope.programList = programList;
    toastr.success(programList.length + " program(s) retrieved",'Operation successful',{timeOut: 2500});
  };

  $scope.handleProgramDeleted = function(newProgramList){
    $scope.programList = newProgramList;
    $scope.selectedProgramIndex = null;
    $scope.trainingProgram = null;
    toastr.success("Program removed",'Operation successful',{timeOut: 2500});
  };

  $scope.getProgramList = function(){
    socket.emit("getProgramsFromDb", "programListData");
  };

  $scope.getUserList = function(){
    socket.emit("getUsersFromDb", "userListData");
  };

  $scope.handleUserListData = function(newUserList){
    $scope.userList = newUserList;
    toastr.success(newUserList.length + " user(s) retrieved",'Operation successful',{timeOut: 2500});
  };

  $scope.handleUserAdded = function(newUserList){
    toastr.success("User added",'Operation successful',{timeOut: 2500});
    $scope.userList = newUserList;
    $scope.showUserInput = false;
    $scope.newUser = {name:""};
  };

  $scope.handleUserDeleted = function(newUserList){
    $scope.userList = newUserList;
    $scope.selectedUser = null;
    toastr.success("User removed",'Operation successful',{timeOut: 2500});
  };

  //-------------------------------------------------------------------------------------------------------------------
  //INIT
  //-------------------------------------------------------------------------------------------------------------------

  $scope.init = function () {
    $scope.showUserInput = false;
    $scope.newUser = {name:""};
    $scope.selectedUser = sharedData.selectedUser;
    $scope.selectedProgramIndex = sharedData.selectedProgramIndex;
    $scope.programList = sharedData.programList;
    $scope.trainingProgram = sharedData.trainingProgram;
    $scope.userList = sharedData.userList;
    if($scope.selectedProgramIndex !== null && $scope.programList !== null){
      //refresh the program content when loading the page to take the updates in account
      $scope.updateTrainingProgram();
    }
    if($scope.programList === null){
      $scope.getProgramList();
    }
    if($scope.userList === null){
      $scope.getUserList();
    }  
  };

  $scope.init();
}]);
//end Home Controller