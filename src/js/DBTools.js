// jshint esversion:6

const fs = require('fs');
const path = require('path');

const Datastore = require('nedb');

//DATABASE
var dbProgramFilePath = path.join(__dirname,"..","resources","programs.db");
var dbProgram = new Datastore({ filename: dbProgramFilePath, autoload: true});
var dbUserFilePath = path.join(__dirname,"..","resources","users.db");
var dbUser = new Datastore({ filename: dbUserFilePath, autoload: true});
var dbHistoryFilePath = path.join(__dirname,"..","resources","history.db");
var dbHistory = new Datastore({ filename: dbHistoryFilePath, autoload: true});


function lookForPrograms(client, response){
  dbProgram.find({}, function (err, programList) {
    if(err){
      console.log(err);
      client.emit("errorMsg","Error while quering the DB for programs.",err);
    }
    else{
      client.emit(response, programList);
      console.log("Server -> client : ", response," nb results: ", programList.length);
    }
  });
}

function writeProgramData(programData, client, response){
  //checking if the element has already a db id
  if(programData._id !== undefined){
    dbProgram.update({_id: programData._id}, programData, function (err, numReplaced) {
      if(err){
        console.log(err);
        client.emit("errorMsg","Error while updating a program in the DB.",err);
      }
      else if(numReplaced !== 1){
          client.emit("errorMsg","Error while updating a program in the DB: "+numReplaced+" program(s) modified, expected 1.",err);
      }
      else{
        //success, we return the new db content
        lookForPrograms(client, response);
      }
    });
  }
  else{
    dbProgram.insert(programData, function (err, newProgram) {
      if(err){
        console.log(err);
        client.emit("errorMsg","Error while inserting a program in the DB.",err);
      }
      else{
        //success, we return the new db content
        lookForPrograms(client, response);
      }
    });
  }
}

function deleteProgram(programId, client, response){
  dbProgram.remove({_id: programId}, function (err, numRemoved) {
    if(err){
      console.log(err);
      client.emit("errorMsg","Error while removing a program from the DB.",err);
    }
    else if(numRemoved !== 1){
        client.emit("errorMsg","Error while removing a program from the DB: "+numRemoved+" program(s) removed, expected 1.",err);
    }
    else{
      //success, we return the new db content
      lookForPrograms(client, response);
    }
  });
}

function lookForUsers(client, response){
  dbUser.find({}, function (err, userList) {
    if(err){
      console.log(err);
      client.emit("errorMsg","Error while quering the DB for user.",err);
    }
    else{
      client.emit(response, userList);
      console.log("Server -> client : ", response," nb results: ", userList.length);
    }
  });
}

function addUser(userData, client, response){
  dbUser.insert(userData, function (err, newUser) {
    if(err){
      console.log(err);
      client.emit("errorMsg","Error while inserting a user in the DB.",err);
    }
    else{
      //success, we return the new db content
      lookForUsers(client, response);
    }
  });
}

function deleteUser(userId, client, response){
  dbUser.remove({_id: userId}, function (err, numRemoved) {
    if(err){
      console.log(err);
      client.emit("errorMsg","Error while removing a user from the DB.",err);
    }
    else if(numRemoved !== 1){
        client.emit("errorMsg","Error while removing a user from the DB: "+numRemoved+" user(s) removed, expected 1.",err);
    }
    else{
      //success, we return the new db content
      lookForUsers(client, response);
    }
  });
}

function lookForHistoryRecords(userId, client, response){
  dbHistory.find({"user._id": userId}, function (err, recordList) {
    if(err){
      console.log(err);
      client.emit("errorMsg","Error while quering the DB for history records.",err);
    }
    else{
      client.emit(response, recordList);
      console.log("Server -> client : ", response," nb results: ", recordList.length);
    }
  });
}

function addHistoryRecord(recordData, client, response){
  dbHistory.insert(recordData, function (err, newRecord) {
    if(err){
      console.log(err);
      client.emit("errorMsg","Error while inserting an history record in the DB.",err);
    }
    else{
      //success, we return the new db content
      lookForHistoryRecords(recordData.user._id, client, response);
    }
  });
}

function deleteHistoryRecord(recordId, userId, client, response){
  dbHistory.remove({_id: recordId}, function (err, numRemoved) {
    if(err){
      console.log(err);
      client.emit("errorMsg","Error while removing an history record from the DB.",err);
    }
    else if(numRemoved !== 1){
        client.emit("errorMsg","Error while removing an history record from the DB: "+numRemoved+" record(s) removed, expected 1.",err);
    }
    else{
      //success, we return the new db content
      lookForHistoryRecords(userId, client, response);
    }
  });
}

module.exports = {
  "lookForPrograms": lookForPrograms,
  "writeProgramData": writeProgramData,
  "deleteProgram": deleteProgram,
  "lookForUsers": lookForUsers,
  "addUser": addUser,
  "deleteUser": deleteUser,
  "addHistoryRecord": addHistoryRecord,
  "lookForHistoryRecords": lookForHistoryRecords,
  "deleteHistoryRecord": deleteHistoryRecord
};