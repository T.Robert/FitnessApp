/*jshint esversion: 6 */

//record Modal Controller
mainApp.controller('recordModalController', ['$scope','sharedData','$uibModalInstance', 'selectedRecord', 'selectedUser',
function($scope, sharedData, $uibModalInstance, selectedRecord, selectedUser){

  //-------------------------------------------------------------------------------------------------------------------
  //TRIGGERED FUNCTIONS
  //-------------------------------------------------------------------------------------------------------------------

  $scope.removeRecord = function(){
    socket.emit("deleteHistoryRecordFromDb", $scope.selectedRecord._id, $scope.selectedUser._id, "recordDeleted");
    $uibModalInstance.dismiss('cancel');
  };

  $scope.closeDialog = function(){
    $uibModalInstance.dismiss('cancel');
  };
  
  //-------------------------------------------------------------------------------------------------------------------
  //TOOLS
  //-------------------------------------------------------------------------------------------------------------------
  

  //-------------------------------------------------------------------------------------------------------------------
  //INIT
  //-------------------------------------------------------------------------------------------------------------------

  $scope.init = function () {
    $scope.selectedRecord = selectedRecord;
    $scope.selectedUser = selectedUser;
  };
  $scope.init();

}]);
//end record Modal Controller